module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "custodio"
  cidr = "10.0.0.0/16"

  azs             = ["eu-west-2a", "eu-west-2b", "eu-west-2c"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Terraform = "true"
    Name      = "custodio"
  }
}

#####################################################################

module "alb_security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "custodio-alb-sg"
  description = "Security group for the client application"
  vpc_id      = module.vpc.vpc_id

  ingress_rules       = ["http-80-tcp"]
  ingress_cidr_blocks = ["0.0.0.0/0"]

  egress_rules       = ["all-all"]
  egress_cidr_blocks = module.vpc.private_subnets_cidr_blocks

  tags = {
    Terraform = "true"
    Name      = "custodio"
  }
}

module "load_balancer" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 7.0"

  name               = "custodio-alb"
  load_balancer_type = "application"

  vpc_id          = module.vpc.vpc_id
  subnets         = module.vpc.public_subnets
  security_groups = [module.alb_security_group.security_group_id]

  http_tcp_listeners = [
    {
      port               = 80 # default for internet, always this one
      protocol           = "HTTP"
      target_group_index = 0
    }
  ]

  target_groups = [
    {
      name             = "custodio-tg"
      backend_protocol = "HTTP"
      backend_port     = 3000
      target_type      = "instance"
      health_check = {
        path    = "/"
        port    = 3000
        matcher = "200-299"
      }
    }
  ]

  http_tcp_listener_rules_tags = {
    Terraform = "true"
    Name      = "custodio"
  }

  tags = {
    Terraform = "true"
    Name      = "custodio"
  }
}

#####################################################################

module "internal_alb_security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "custodio-internal-alb-sg"
  description = "Security group for the internal services"
  vpc_id      = module.vpc.vpc_id

  ingress_rules       = ["http-80-tcp"]
  ingress_cidr_blocks = ["10.0.0.0/16"]

  egress_rules       = ["all-all"]
  egress_cidr_blocks = module.vpc.private_subnets_cidr_blocks

  tags = {
    Terraform = "true"
    Name      = "custodio"
  }
}

module "internal_load_balancer" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 7.0"

  name               = "custodio-internal-alb"
  internal           = true
  load_balancer_type = "application"

  vpc_id          = module.vpc.vpc_id
  subnets         = module.vpc.public_subnets
  security_groups = [module.internal_alb_security_group.security_group_id]

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 0
    }
  ]

  target_groups = [
    {
      name             = "custodio-internal-tg"
      backend_protocol = "HTTP"
      backend_port     = 3000
      target_type      = "ip"
      health_check = {
        path    = "/"
        port    = 3000
        matcher = "200-299"
      }
    }
  ]

  http_tcp_listener_rules_tags = {
    Terraform = "true"
    Name      = "custodio"
  }

  tags = {
    Terraform = "true"
    Name      = "custodio"
  }
}

#####################################################################

resource "aws_route53_zone" "private" {
  name = "custodio.local"

  vpc {
    vpc_id = module.vpc.vpc_id
  }

  tags = {
    Terraform = "true"
    Name      = "custodio"
  }
}

resource "aws_route53_record" "internal" {
  zone_id = aws_route53_zone.private.zone_id
  name    = "custodio"
  type    = "CNAME"
  records = [module.internal_load_balancer.lb_dns_name]
  ttl     = 300
}

