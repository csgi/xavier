output "vpc_id" {
  description = "The ID of the VPC"
  value       = module.vpc.vpc_id
}

output "private_subnets" {
  description = "A list of private subnets"
  value       = module.vpc.private_subnets
}

output "public_subnets" {
  description = "A list of public subnets"
  value       = module.vpc.public_subnets
}

output "private_subnets_cidr_blocks" {
  description = "A list of private subnets cidr blocks"
  value       = module.vpc.private_subnets_cidr_blocks
}

output "target_group_arns" {
  value       = module.load_balancer.target_group_arns
  description = "ARNs of the target groups. Useful for passing to your Auto Scaling group."
}

output "internal_target_group_arns" {
  value       = module.internal_load_balancer.target_group_arns
  description = "ARNs of the target groups. Useful for passing to your Auto Scaling group."
}

output "alb_security_group_id" {
  value       = module.alb_security_group.security_group_id
  description = "Alb security group id"
}

output "internal_alb_security_group_id" {
  value       = module.internal_alb_security_group.security_group_id
  description = "Internal Alb security group id"
}

