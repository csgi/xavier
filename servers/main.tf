module "bastion_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "custodio-bastion-sg"
  description = "Custodio bastion security group"
  vpc_id      = var.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["ssh-tcp"]

  egress_rules = ["all-all"]

  tags = {
    Name      = "Bastion Security Group"
    Terraform = true
  }
}

resource "aws_instance" "bastion_host" {
  ami                         = "ami-0eb260c4d5475b901"
  instance_type               = "t3.micro"
  subnet_id                   = element(var.public_subnets, 1)
  security_groups             = [module.bastion_sg.security_group_id]
  associate_public_ip_address = true
  monitoring                  = true
  key_name                    = "digicel-squidex"

  # csg demands these tags
  tags = {
    Name          = "Custodio Bastion host"
    SecurityClass = "C"
    Customers     = "CSG International"
    ServiceLevel  = "Development"
    ProductCode   = 7500
    RemedyGroup   = "Experiences Dev GLOBAL L3"
    Application   = "Bastion Host"
    Function      = "Bastion Host"
    Terraform     = true
  }
}

module "web_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "web-sg"
  description = "Web security group"
  vpc_id      = var.vpc_id

  ingress_with_source_security_group_id = [
    {
      from_port                = 22
      to_port                  = 22
      protocol                 = "tcp"
      source_security_group_id = module.bastion_sg.security_group_id
    },
  ]
  egress_rules = ["all-all"]

  tags = {
    Name      = "Web host Security Group"
    Terraform = true
  }
}

module "alb_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "web-alb-sg"
  description = "Alb security group"
  vpc_id      = var.vpc_id

  ingress_with_source_security_group_id = [
    {
      from_port                = 3000
      to_port                  = 3000
      protocol                 = "tcp"
      source_security_group_id = var.alb_security_group_id
    },
  ]

  egress_rules = ["all-all"]

  tags = {
    Name      = "ALB Security Group"
    Terraform = true
  }
}

resource "aws_instance" "web" {
  ami             = "ami-0eb260c4d5475b901" # ubuntu
  instance_type   = "t3.micro"
  subnet_id       = element(var.private_subnets, 1)
  security_groups = [module.web_sg.security_group_id, module.alb_sg.security_group_id]
  monitoring      = true
  key_name        = "digicel-squidex"

  # csg demands these tags
  tags = {
    Name          = "Web host"
    SecurityClass = "C"
    Customers     = "CSG International"
    ServiceLevel  = "Development"
    ProductCode   = 7500
    RemedyGroup   = "Experiences Dev GLOBAL L3"
    Application   = "Web"
    Function      = "Web"
    Terraform     = true
  }
}

resource "aws_lb_target_group_attachment" "alb_web" {
  target_group_arn = element(var.target_group_arns, 0)
  target_id        = aws_instance.web.id
  port             = 3000
}


module "internal_web_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "internal-web-sg"
  description = "Web security group"
  vpc_id      = var.vpc_id

  ingress_with_source_security_group_id = [
    {
      from_port                = 22
      to_port                  = 22
      protocol                 = "tcp"
      source_security_group_id = module.bastion_sg.security_group_id
    },
  ]
  egress_rules = ["all-all"]

  tags = {
    Name      = "Internal Web host Security Group"
    Terraform = true
  }
}

module "internal_alb_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "internal-web-alb-sg"
  description = "Internal Alb security group"
  vpc_id      = var.vpc_id

  ingress_with_source_security_group_id = [
    {
      from_port                = 3000
      to_port                  = 3000
      protocol                 = "tcp"
      source_security_group_id = var.internal_alb_security_group_id
    },
  ]

  egress_rules = ["all-all"]

  tags = {
    Name      = "ALB Security Group"
    Terraform = true
  }
}

resource "aws_instance" "internal_web" {
  ami             = "ami-0eb260c4d5475b901" # ubuntu
  instance_type   = "t3.micro"
  subnet_id       = element(var.private_subnets, 1)
  security_groups = [module.internal_web_sg.security_group_id, module.internal_alb_sg.security_group_id]
  monitoring      = true
  key_name        = "digicel-squidex"

  # csg demands these tags
  tags = {
    Name          = "Web host"
    SecurityClass = "C"
    Customers     = "CSG International"
    ServiceLevel  = "Development"
    ProductCode   = 7500
    RemedyGroup   = "Experiences Dev GLOBAL L3"
    Application   = "Web"
    Function      = "Web"
    Terraform     = true
  }
}

resource "aws_lb_target_group_attachment" "internal_alb_web" {
  target_group_arn = element(var.internal_target_group_arns, 0)
  target_id        = aws_instance.internal_web.id
  port             = 3000
}
