variable "vpc_id" {
  description = "The ID of the VPC"
}

variable "private_subnets" {
  description = "A list of private subnets"
}
variable "public_subnets" {
  description = "A list of public subnets"
}

variable "private_subnets_cidr_blocks" {
  description = "A list of private subnets cidr blocks"
}

variable "target_group_arns" {
  description = "ARNs of the target groups. Useful for passing to your Auto Scaling group."
}

variable "internal_target_group_arns" {
  description = "ARNs of the target groups. Useful for passing to your Auto Scaling group."
}

variable "alb_security_group_id" {
  description = "Alb security group id"
}

variable "internal_alb_security_group_id" {
  description = "Internal Alb security group id"
}
