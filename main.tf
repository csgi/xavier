module "network" {
  source = "./network"
}

module "servers" {
  source                         = "./servers"
  vpc_id                         = module.network.vpc_id
  private_subnets                = module.network.private_subnets
  public_subnets                 = module.network.public_subnets
  private_subnets_cidr_blocks    = module.network.private_subnets_cidr_blocks
  target_group_arns              = module.network.target_group_arns
  internal_target_group_arns     = module.network.internal_target_group_arns
  alb_security_group_id          = module.network.alb_security_group_id
  internal_alb_security_group_id = module.network.internal_alb_security_group_id
}
